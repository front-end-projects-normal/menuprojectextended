const menu = [
  {
    id: 1,
    title: "buttermilk pancakes",
    category: "breakfast",
    price: 15.99,
    in_cart: false,
    button: "./images/cartlogo.webp",
    greencart: "./images/greencart.png",
    img: "./images/item-1.jpeg",
    desc: `I'm baby woke mlkshk wolf bitters live-edge blue bottle, hammock freegan copper mug whatever cold-pressed `,
  },
  {
    id: 2,
    title: "diner double",
    category: "lunch",
    price: 13.99,
    button: "./images/cartlogo.webp",
    greencart: "./images/greencart.png",
    img: "./images/item-2.jpeg",
    desc: `vaporware iPhone mumblecore selvage raw denim slow-carb leggings gochujang helvetica man braid jianbing. Marfa thundercats `,
  },
  {
    id: 3,
    title: "godzilla milkshake",
    category: "shakes",
    price: 6.99,
    in_cart: false,
    button: "./images/cartlogo.webp",
    greencart: "./images/greencart.png",
    img: "./images/item-3.jpeg",
    desc: `ombucha chillwave fanny pack 3 wolf moon street art photo booth before they sold out organic viral.`,
  },
  {
    id: 4,
    title: "country delight",
    category: "breakfast",
    price: 20.99,
    button: "./images/cartlogo.webp",
    greencart: "./images/greencart.png",
    img: "./images/item-4.jpeg",
    desc: `Shabby chic keffiyeh neutra snackwave pork belly shoreditch. Prism austin mlkshk truffaut, `,
  },
  {
    id: 5,
    title: "egg attack",
    category: "lunch",
    price: 22.99,
    in_cart: false,
    button: "./images/cartlogo.webp",
    greencart: "./images/greencart.png",
    img: "./images/item-5.jpeg",
    desc: `franzen vegan pabst bicycle rights kickstarter pinterest meditation farm-to-table 90's pop-up `,
  },
  {
    id: 6,
    title: "oreo dream",
    category: "shakes",
    price: 18.99,
    button: "./images/cartlogo.webp",
    greencart: "./images/greencart.png",
    img: "./images/item-6.jpeg",
    desc: `Portland chicharrones ethical edison bulb, palo santo craft beer chia heirloom iPhone everyday`,
  },
  {
    id: 7,
    title: "bacon overflow",
    category: "breakfast",
    price: 8.99,
    in_cart: false,
    button: "./images/cartlogo.webp",
    greencart: "./images/greencart.png",
    img: "./images/item-7.jpeg",
    desc: `carry jianbing normcore freegan. Viral single-origin coffee live-edge, pork belly cloud bread iceland put a bird `,
  },
  {
    id: 8,
    title: "american classic",
    category: "lunch",
    price: 12.99,
    in_cart: false,
    button: "./images/cartlogo.webp",
    greencart: "./images/greencart.png",
    img: "./images/item-8.jpeg",
    desc: `on it tumblr kickstarter thundercats migas everyday carry squid palo santo leggings. Food truck truffaut  `,
  },
  {
    id: 9,
    title: "quarantine buddy",
    category: "shakes",
    price: 16.99,
    in_cart: false,
    button: "./images/cartlogo.webp",
    greencart: "./images/greencart.png",
    img: "./images/item-9.jpeg",
    desc: `skateboard fam synth authentic semiotics. Live-edge lyft af, edison bulb yuccie crucifix microdosing.`,
  },
  {
    id: 10,
    title: "bison steak",
    category: "dinner",
    price: 22.99,
    in_cart: false,
    button: "./images/cartlogo.webp",
    greencart: "./images/greencart.png",
    img: "./images/item-10.jpeg",
    desc: `skateboard fam synth authentic semiotics. Live-edge lyft af, edison bulb yuccie crucifix microdosing.`,
  },

];

// get parent element
//queryselector is returing the value of the section-center and btn-container
const sectionCenter = document.querySelector(".section-center");
const btnContainer = document.querySelector(".btn-container");
// display all items when page loads
//addeventlistner is using as event return property
//DOMcontentLoaded is used when the inital HTML document has to be change
window.addEventListener("DOMContentLoaded", function () {
  //Displaymenuitems is used to display the map structure of the Cards UI and info
  diplayMenuItems(menu);
  //displaymenubuttons is used to display the map structure of the Button UI and Info
  displayMenuButtons();
});

//This function is displaying Items using meuItems as parameters
function diplayMenuItems(menuItems) {
    //map create a structure and a new array for each key from the existing main Array
  let displayMenu = menuItems.map(function (item) {
    // console.log(item);
    //return is returing the value(Structure of DOM)
    return `<article class="menu-item">
          <img src=${item.img} alt=${item.title} class="photo" />
          <div class="item-info">
            <header>
              <h4>${item.title}</h4>
              <h4 class="price">$${item.price}</h4>
              <div class="addtocart" id="img-${item.id}" onclick="addToCart(${item.id})"><img src="${item.button}" height ="20" width="50"/></div>
            </header>
            <p class="item-text">
              ${item.desc}
            </p>
          </div>
        </article>`;
  });

  // join() method creates and returns a new string by concatenating all of the elements in an array
  displayMenu = displayMenu.join("");
  // console.log(displayMenu);  


  sectionCenter.innerHTML = displayMenu;
}
// menu.reduce is used to pop the unwanted filtered category
function displayMenuButtons() {
  // reduce() method executes a reducer function for array element
  const categories = menu.reduce(
    function (values, item) {
      // includes() method returns true if a string contains a specified string
      if (!values.includes(item.category)) {
        // push() method adds one or more elements to the end of an array and returns the new length of the array
        values.push(item.category);
      }
      return values;
    },
    ["all"]
  );
  //map create a structure and a new array for each key from the existing main Array
  const categoryBtns = categories
    .map(function (category) {
      return `<button type="button" class="filter-btn" data-id=${category}>
          ${category}
        </button>`;
    })
      // join() method creates and returns a new string by concatenating all of the elements in an array
    .join("");


    //innerHTML returns text content of the element
  btnContainer.innerHTML = categoryBtns;
  // querySelectorAll() method allows you to select the element that matches CSS selectors
  const filterBtns = btnContainer.querySelectorAll(".filter-btn");
  //console.log is used to print values in Browser console
  console.log(filterBtns);

  //forEach() method calls a function for each element in an array
  filterBtns.forEach(function (btn) {
    // addEventListener() method attaches an event handler to a document

    btn.addEventListener("click", function (e) {
      // console.log(e.currentTarget.dataset);
      //currentTarget event property returns the element whose event listeners triggered the event
      // "e" is the object handler |event handling function
      const category = e.currentTarget.dataset.id;
      // filter() method creates a new array filled with elements that pass a test provided by a function
      const menuCategory = menu.filter(function (menuItem) {
        // console.log(menuItem.category);
        if (menuItem.category === category) {
          return menuItem;
        }
      });
      if (category === "all") {
        diplayMenuItems(menu);
      } else{
        diplayMenuItems(menuCategory);
      }
      
    });
  });
}

// add to cart
let cart = [];

let cartBtn = document.getElementById("cartbtn");

function addToCart(id){
  // console.log(id)
  const cartItems = menu.find((item) => item.id === id)

  // console.log(cartItems)

  cart.push(cartItems);
  console.log(cart);
  document.getElementById(`img-${id}`).src = `${item.greencart}`;
}

cartBtn.onclick = function(){
  diplayMenuItems(cart);

}
